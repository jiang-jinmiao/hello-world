//页面刷新后请求数据
// $.ajax({
//     url: 'http://192.168.1.105:8080/OAProject/regist',
//     type: 'post',
//     dataType: 'json',
//     success:function (res) {
//         if(res['message'] =='注册成功'){
//             console.log('请求成功')
//         }else{
//             console.log('请求失败')
//         }
//     }
// });
// 第一个表单姓名
var userName = /^[\u4E00-\u9FA5]{2,3}$/;                            //获取正则
$("input[ name='uname']").blur(function () {
    if (!userName.test($(this).val())) {                               //判断正则
        $(this).css("outline", "none");                                 //取消点击表单默认样式
        $(this).css("border-color", "red");                                 
        $(this).parent().nextAll(".errorInfor").show();                     //警告文字出现
    } else{                                                                  //符合正则的判断
       $(this).css("border-color", "#dcdfe6");
        $(this).parent().nextAll(".errorInfor").hide();                       //隐藏书写错误的警告文字
        $(this).parent().nextAll(".errorNull").hide();                      //消除空白注册带来的警告文字
    } 
});


// 第二个表单手机号
 var mobilenumber =  /^[1][3,4,5,7,8,9][0-9]{9}$/;
$("input[ name='Iphone']").blur(function () {
    if (!mobilenumber.test($(this).val())) {
        $(this).css("outline", "none");
        $(this).css("border-color", "red");
        $(this).parent().nextAll(".errorInfor").show();
    } else{
        $(this).css("border-color", "#dcdfe6")
        $(this).parent().nextAll(".errorInfor").hide();
        $(this).parent().nextAll(".errorNull").hide();
    }
});


// 第三个表单邮箱
 var postbox = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
$("input[name='Email']").blur(function () {
    if (!postbox.test($(this).val())) {
        $(this).css("outline", "none");
        $(this).css("border-color", "red");
        $(this).parent().nextAll(".errorInfor").show();
    } else{
        $(this).css("border-color", "#dcdfe6")
        $(this).parent().nextAll(".errorInfor").hide();
        $(this).parent().nextAll(".errorNull").hide();
    }
});


// 第四个表单密码
var cypher = /^[a-z0-9_-]{6,18}$/;
$("input[name='upassword']").blur(function () {
    if (!cypher.test($(this).val())) {
        $(this).css("outline", "none");
        $(this).css("border-color", "red");
        $(this).parent().nextAll(".errorInfor").show();
    } else{
        $(this).css("border-color", "#dcdfe6")
        $(this).parent().nextAll(".errorInfor").hide();
        $(this).parent().nextAll(".errorNull").hide();
    }
});


// 第五个表单验证密码
$("input[name='again_pass']").keyup(function () {
    if ($(this).val() != $("#form4").val()) {
        $(this).css("outline", "none");
        $(this).css("border-color", "red");                      //隐藏空白注册带带来的警告文字，由于是键盘抬起事件，放在else=没办法第一时间消除，导致制出现两行文字
        $(this).parent().nextAll(".errorInfor").show();
        $(this).parent().nextAll(".errorNull").hide();
    } else if ($(this).val() == $("#form4").val()) {
        $(this).css("border-color", "#409eff")
        $(this).parent().nextAll(".errorInfor").hide();
    }
});


// 消掉立即注册改变的边框颜色     
$("input[ name='again_pass']").blur(function () {
    if ($(this).val() == $("#form4").val()) {
        $("#form5").css("border-color", "#dcdfe6");
    }
});


// 第六个表单验证码
// $("input[name='experimental']").blur(function () {
//     if ($(this).val() != 10086) {                          //判断是否等于 验证图片上的信息
//         $(this).parent().nextAll(".errorInfor").show();
//         $(this).css("border-color", "red");
        
//     } else if ($(this).val() == 10086) {
//         $(this).parent().nextAll(".errorInfor").hide();
//         $(this).css("border-color", "#dcdfe6");
//          $(this).parent().nextAll(".errorNull").hide();             
//     }
// });


$(".read").click(function () {     //点击多选框
    if (this.checked) {             //如果被选中“read”等于hcecked  遮罩层消失，注册按钮颜色改变;
        $(".Mask").hide();
        $("#loginBtn").css("background-color", "#409eff");
        $("#loginBtn").click(function () {
            var Rname = $("input[ name='uname']").val();
            var Riphone = $("input[ name='Iphone']").val();
            var Remail = $("input[ name='Email']").val();
            var Rpassword = $("input[ name='upassword']").val();
            var Inp = $("input");
            // 利用for循环检测每个表单框里的字符长度是否为零，有为零的改变边框颜色，出现红色字体警告。不为零边框不变，字体不出现
            for (var index = 0; index < Inp.length; index++) {
                if ($("input").eq(index).val().length == 0) {
                    $("input").eq(index).css("border-color", "red");
                    $("input").eq(index).parent().nextAll(".errorNull").show();
                } 
            }
            // 判断上面的表单框里的值格式是否正确，正确弹出“注册成功”，否则弹出格式错误
            if(userName.test($("input[ name='uname']").val()) && mobilenumber.test($("input[ name='Iphone']").val())&& 
            postbox.test($("input[name='Email']").val())&& cypher.test($("input[name='upassword']").val())&&
            $("input[name='again_pass']").val() == $("#form4").val() ){
                    $.ajax({
                        url: 'http://192.168.1.105:8080/OAProject/regist',
                        type: 'post',
                        data: {
                            'empName': Rname,
                            'empPassword': Rpassword,
                            'empEmail': Remail,
                            'empPhone': Riphone
                        },
                        dataType: 'json',
                        success:function (res) {
                            if(res['message'] =='注册成功'){
                                alert("注册成功00");
                                window.location.href = "./login.html";
                            }else{
                                alert('注册失败');
                            }
                        },
                        // error: function(error){
                        //     console.error('error');
                        // }
                    });
                    }else {
                        alert("格式错误");
                    }
            });
    }
    else {            //如果取消选中“read” 遮罩层出现，注册按钮颜色改变;
        $(".Mask").show();
        $("#loginBtn").css("background-color", "#a0cfff");
    }
});


// 改变注册鼠标样式
$("#loginBtn").mouseover(function(){
    $(this).css("cursor","Pointer");
});
// 移入·背景改变
$("#loginBtn").mouseover(function(){
     $(this).css("background-color", "#66b1ff");
});
// 移出样式改变
$("#loginBtn").mouseout(function(){
    $(this).css("background-color", "#409eff");
});

// 移入注册上边遮罩层鼠标样式改变
$(".Mask").mouseover(function(){
    $(this).css("cursor","url(img/register1.png) 5 5,pointer");
});


// 重置按钮
$("#resetBtn").click(function(){
    $("input").val("");
    $("input").parent().nextAll(".errorNull").hide();
    $("input").parent().nextAll(".errorInfor").hide();
    $("input").css("border-color", "#dcdfe6");
});


// 移入重置按钮鼠标样式，边框颜色，背景颜色改变
$("#resetBtn").mouseover(function(){
    $(this).css("cursor","Pointer");
    $(this).css("border-color", " #a0cfff");
     $(this).css("background-color", "#ecf5ff");
    $(this).css("color", "#409eff");
});
// 移出重置按钮鼠标样式，边框颜色，背景颜色改变
$("#resetBtn").mouseout(function(){
    $(this).css("cursor","Pointer");
    $(this).css("border-color", "darkgrey");
    $(this).css("background-color", "#ffffff");
    $(this).css("color", "black");
});

//  鼠标按下重置按钮边框颜色改变
$("#resetBtn").mousedown(function(){
    $(this).css("border-color", "#3a8ee6");
});
// 鼠标抬起重置按钮边框颜色改变
$("#resetBtn").mouseup(function(){
    $(this).css("border-color", "#a0cfff"); 
});

function changeVerifyCode(img){
    console.log(1)
    img.src="http://192.168.1.63:8080/HomeWork_Day_06/kaptcha?" + Math.floor(Math.random()*100);
}
var img1 = $('#captcha_img');
changeVerifyCode(img1);