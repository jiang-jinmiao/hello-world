// $.ajax({
//     url: 'xxx.com',
//     type: 'post',
//     data: {

//     }
// })
// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementsByClassName('echart_pic')[0]);
// 指定图表的配置项和数据
var option1 = {
    title: {
        text: '年度月份报名量'
    },
    tooltip: {},
    
    xAxis: {
        data: ["1月","2月","3月","4月","5月","6月","7月","8月"]
    },
    yAxis: {},
    series: [{
        name: '报名量',
        type: 'bar',
        data: [15, 20, 36, 15, 15, 20, 40, 56]
    }]
};

// 使用刚指定的配置项和数据显示图表。


$('.echart_li>li').click(function(){
    var index=$(this).index();
    $(this).addClass('active_li').siblings().removeClass('active_li');
    if(index == 0){
        option1.series[0].type = 'bar';
        option1.title.text = '年度月份报名量';
        option1.series[0].data = [15, 20, 36, 15, 15, 20, 40, 56]
        myChart.setOption(option1);
    }else if(index == 1){
        option1.series[0].type = 'line';
        option1.title.text = '年度月份咨询量';
        option1.series[0].data = [20, 30, 40, 20, 30, 40, 50, 60]
        myChart.setOption(option1);
    }
    else{
        option1.series[0].type = 'bar';
        option1.title.text = '年度月份退费量';
        option1.series[0].data = [10, 5, 40, 20, 30, 40, 50, 30]
        myChart.setOption(option1);
    }
})
myChart.setOption(option1);