//用户名和二维码页面跳转
$('h2>span').click(function(){
    var index=$(this).index();
    $(this).addClass("active")
    .siblings().removeClass("active")
    .parent().next().children().eq(index).show().siblings().hide();
});

//密码的显示与隐藏
$('.form-Ipt>em').click(function(){
    var index=$(this).index();
    $(this).hide().siblings().show();
    if(index == 1){
        $('#pass').attr('type','text');
    }else{
        $('#pass').attr('type','password');
    }
})

//登录请求发送
$('#loginBtn').click(function(){
    var phone = $('#user-name').val();
    var pass = $("input[ name='password']").val();
    console.log(phone,pass);
    $.ajax({
        url: 'http://192.168.1.105:8080/OAProject/login',
        type: 'post',
        data: {
            'empPhone': phone,
            'empPassword': pass
        },
        dataType: 'json',
        success: function(res){
            if(res['message'] =='登录成功'){
                alert("登录成功");
                location.href = "./home.html";
            }else{
                alert('登录失败');
            }
        }
    })
});

//注册页面的跳转
$('#registerBtn').click(function(){
    location.href = './register.html';
});
