/*
 * @Author: RenJihong
 * @Date: 2020-09-17 09:16:24
 */
layui.use('laydate', function(){
  var laydate = layui.laydate;
  
  laydate.render({
    elem: '#date1'
  });
  laydate.render({
    elem: '#date2'
  });
});
layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;
});

var yeji_chart = echarts.init(document.getElementsByClassName('echart_pictu')[0]);
var option2 = {
  tooltip: {
      trigger: 'axis',
      axisPointer: {
          type: 'cross',
          crossStyle: {
              color: '#999'
          }
      }
  },
  legend: {
      data: ['报名量', '咨询量', '转化率']
  },
  xAxis: [
      {
          type: 'category',
          data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
          axisPointer: {
              type: 'shadow'
          }
      }
  ],
  yAxis: [
      {
          type: 'value',
          name: '人数(人)',
          min: 0,
          max: 100,
          interval: 20,
      },
      {
          type: 'value',
          name: '百分比',
          min: 0,
          max: 100,
          interval: 20,
          axisLabel: {
              formatter: '{value} %'
          }
      }
  ],
  series: [
      {
          name: '报名量',
          type: 'bar',
          data: [20, 49, 70, 23, 25, 76, 56, 22, 26, 80, 64, 33]
      },
      {
          name: '咨询量',
          type: 'bar',
          data: [26, 59, 90, 24, 37, 87, 66, 32, 47, 88, 68, 43]
      },
      {
          name: '转化率',
          type: 'line',
          yAxisIndex: 1,
          data: [2, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
      }
  ]
};
yeji_chart.setOption(option2);





// function creatLi() {  
//     var Li = '<li></li>';
//     var Div = '<div></div>';
//     $('#scroll_ul').append(Li);
//     $('#scroll_ul li').append(Div);
//     $('#scroll_ul li div:eq(0)').addClass('content_li_left').html('123');
//     $('#scroll_ul li div:eq(1)').addClass('content_li_center').html('456');
//     $('#scroll_ul li div:eq(2)').addClass('content_li_right').html('789');
// }
for(var i = 0; i < 4; i++ ){
    $('#scroll_ul').append('<li><div class = "content_li_left">222</div><div class = "content_li_center">3333</div> <div class = "content_li_right"> 10 - 15 </div></li>');
} 
// $('#scroll_ul').
